import './stencil.core';
/**
 * This is an autogenerated file created by the Stencil build process.
 * It contains typing information for all components that exist in this project
 * and imports for stencil collections that might be configured in your stencil.config.js file
 */

import './stencil.core';

declare global {
  namespace JSX {
    interface Element {}
    export interface IntrinsicElements {}
  }
  namespace JSXElements {}

  interface HTMLStencilElement extends HTMLElement {
    componentOnReady(): Promise<this>;
    componentOnReady(done: (ele?: this) => void): void;

    forceUpdate(): void;
  }

  interface HTMLAttributes {}
}


declare global {

  namespace StencilComponents {
    interface RgpdComponent {
      'opcionesCheck': any[];
      'showModal': boolean;
      'texto': string;
      'titulo': string;
    }
  }

  interface HTMLRgpdComponentElement extends StencilComponents.RgpdComponent, HTMLStencilElement {}

  var HTMLRgpdComponentElement: {
    prototype: HTMLRgpdComponentElement;
    new (): HTMLRgpdComponentElement;
  };
  interface HTMLElementTagNameMap {
    'rgpd-component': HTMLRgpdComponentElement;
  }
  interface ElementTagNameMap {
    'rgpd-component': HTMLRgpdComponentElement;
  }
  namespace JSX {
    interface IntrinsicElements {
      'rgpd-component': JSXElements.RgpdComponentAttributes;
    }
  }
  namespace JSXElements {
    export interface RgpdComponentAttributes extends HTMLAttributes {
      'onRgpdSigned'?: (event: CustomEvent) => void;
      'opcionesCheck'?: any[];
      'showModal'?: boolean;
      'texto'?: string;
      'titulo'?: string;
    }
  }
}

declare global { namespace JSX { interface StencilJSX {} } }
