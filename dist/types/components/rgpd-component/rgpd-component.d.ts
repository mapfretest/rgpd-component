import '../../stencil.core';
import { EventEmitter } from '../../stencil.core';
export declare class Rgpd {
    titulo: string;
    texto: string;
    opcionesCheck: any[];
    showModal: boolean;
    condicionesRGPD: any;
    rgpd: HTMLElement;
    private boton;
    rgpdSigned: EventEmitter;
    componentDidLoad(): void;
    stringControl(texto: any): any;
    showText: boolean;
    showTextComplete(texto: any): JSX.Element;
    enviarForm(): void;
    isButtonDisabled(condiciones: any): void;
    handleChange(event: any, opt: any): void;
    render(): JSX.Element;
}
