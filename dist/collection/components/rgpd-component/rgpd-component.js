export class Rgpd {
    constructor() {
        this.condicionesRGPD = {};
        this.showText = false;
    }
    componentDidLoad() {
        this.condicionesRGPD = this.opcionesCheck;
        this.isButtonDisabled(this.condicionesRGPD);
        // this.boton =  this.rgpd.shadowRoot.querySelector('.btn-acept');
        // this.boton.setAttribute("disabled", "disabled");
    }
    stringControl(texto) {
        if (texto.length > 50) {
            return (texto.substr(0, 50));
        }
        else {
            return texto;
        }
    }
    showTextComplete(texto) {
        return (h("div", { class: "textComplete" },
            h("p", null, texto)));
    }
    enviarForm() {
        this.rgpdSigned.emit(this.condicionesRGPD);
        let modalRGPD = this.rgpd.shadowRoot.querySelector('.modal');
        modalRGPD.style.display = 'none';
    }
    isButtonDisabled(condiciones) {
        // console.log(condiciones);
        this.boton = this.rgpd.shadowRoot.querySelector('.btn-acept');
        let error = 0;
        for (let op in condiciones) {
            if (condiciones[op].required == true && condiciones[op].status == false) {
                error++;
            }
        }
        if (error > 0 || condiciones.length == undefined) {
            this.boton.removeAttribute('disabled');
            this.boton.setAttribute('disabled', 'disasbled');
        }
        else {
            this.boton.removeAttribute('disabled');
        }
    }
    handleChange(event, opt) {
        this.condicionesRGPD = opt;
        let target = event.target;
        let checked = target.checked;
        let value = target.value;
        for (var opcion in this.condicionesRGPD) {
            if (this.condicionesRGPD[opcion].id == value) {
                this.condicionesRGPD[opcion].status = checked;
            }
        }
        this.isButtonDisabled(this.condicionesRGPD);
    }
    render() {
        if (this.showModal) {
            return (h("div", { id: "rgpd", class: "modal" },
                h("div", { class: "center" },
                    h("h1", null, this.titulo)),
                h("div", { class: "left" },
                    h("p", null, this.texto)),
                h("div", { class: "left" },
                    h("ul", null, this.opcionesCheck.map((opcion) => h("li", null,
                        h("label", null,
                            h("input", { type: "checkbox", name: "conditions", value: opcion.id, required: opcion.required, checked: opcion.status, onChange: (event) => this.handleChange(event, this.opcionesCheck) }),
                            this.stringControl(opcion.text),
                            opcion.text.length > 50 && h("a", { class: "aviso", onClick: () => this.showText = !this.showText }, "Aviso Privacidad")),
                        this.showText && opcion.text.length > 50 && this.showTextComplete(opcion.text)))),
                    h("div", { class: "center" },
                        h("button", { type: "submit", class: "btn-acept", onClick: () => this.enviarForm() }, "ACEPTO")))));
        }
    }
    static get is() { return "rgpd-component"; }
    static get encapsulation() { return "shadow"; }
    static get properties() { return { "opcionesCheck": { "type": "Any", "attr": "opciones-check" }, "rgpd": { "elementRef": true }, "showModal": { "type": Boolean, "attr": "show-modal" }, "showText": { "state": true }, "texto": { "type": String, "attr": "texto" }, "titulo": { "type": String, "attr": "titulo" } }; }
    static get events() { return [{ "name": "rgpdSigned", "method": "rgpdSigned", "bubbles": true, "cancelable": true, "composed": true }]; }
    static get style() { return "/**style-placeholder:rgpd-component:**/"; }
}
